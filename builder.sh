#!/bin/sh
source $stdenv/setup
xwin --accept-license --cache-dir=xwin-out --manifest=$manifest splat
mkdir $out/
mv xwin-out/splat/* $out/
