{
  description = "Here be heinous crimes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      rust-overlay,
      naersk,
      nixpkgs,
      pre-commit-hooks,
      ...
    }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ rust-overlay.overlays.default ];
      };

      crossToolchain = pkgs.rust-bin.stable.latest.default.override {
        extensions = [
          "clippy"
          "rust-src"
        ];
        targets = [ "x86_64-pc-windows-msvc" ];
      };

      crossNaersk = naersk.lib.x86_64-linux.override {
        cargo = crossToolchain;
        rustc = crossToolchain;
      };

      xwin = pkgs.rustPlatform.buildRustPackage rec {
        pname = "xwin";
        version = "0.6.5";

        src = pkgs.fetchFromGitHub {
          owner = "Jake-Shadle";
          repo = pname;
          rev = version;
          hash = "sha256-RAZ58ooKaXWnGkIr6ogfByDRMTrIEUNEVOrUPIrW0PU=";
        };

        useFetchCargoVendor = true;
        cargoHash = "sha256-nxZ6keV6CgzMVPUlEC421xiTQ7Euf43rpAykcxB03k8=";

        # Tests access the network
        checkFlags = [
          "--skip verify_compiles"
          "--skip verify_deterministic"
        ];
      };

      winsdk =
        pkgs.runCommand "windows-sdk"
          {
            buildInputs = [ xwin ];

            manifest = ./manifest.json;

            outputHashMode = "recursive";
            outputHashAlgo = "sha256";
            outputHash = "sha256-bMS9gZ43tziL1t4ph84TYfDM8SLhRXtBibBqRLV/6Xc=";
          }
          ''
            xwin --accept-license --cache-dir=xwin-out --manifest=$manifest splat
            mkdir $out/
            mv xwin-out/splat/* $out/
          '';

      commonEnv = rec {
        CARGO_BUILD_TARGET = "x86_64-pc-windows-msvc";

        CC_x86_64_pc_windows_msvc = "clang-cl";
        CXX_x86_64_pc_windows_msvc = "clang-cl";
        AR_x86_64_pc_windows_msvc = "llvm-lib";
        CFLAGS_x86_64_pc_windows_msvc = "/imsvc${winsdk}/crt/include /imsvc${winsdk}/sdk/include/ucrt /imsvc${winsdk}/sdk/include/um /imsvc${winsdk}/sdk/include/shared";
        CXXFLAGS_x86_64_pc_windows_msvc = CFLAGS_x86_64_pc_windows_msvc;

        CARGO_TARGET_X86_64_PC_WINDOWS_MSVC_LINKER = "lld-link";
        RUSTFLAGS = "-Lnative=${winsdk}/crt/lib/x86_64 -Lnative=${winsdk}/sdk/lib/um/x86_64 -Lnative=${winsdk}/sdk/lib/ucrt/x86_64";

        RUST_SRC_PATH = "${crossToolchain}/lib/rustlib/src/";
      };

      mkCrossPackage =
        args:
        crossNaersk.buildPackage (
          args
          // commonEnv
          // {
            buildInputs = (args.buildInputs or [ ]) ++ [
              pkgs.llvmPackages_latest.lld
              pkgs.llvmPackages_latest.clang-unwrapped
              pkgs.llvmPackages_latest.bintools
              winsdk
            ];
          }
        );
      mkCrossShell = args: pkgs.mkShell (args // commonEnv);

      mkNativePackage = crossNaersk.buildPackage;
    in
    {
      lib = {
        inherit
          mkCrossPackage
          mkCrossShell
          mkNativePackage
          crossToolchain
          ;
      };

      checks.x86_64-linux.pre-commit-check = pre-commit-hooks.lib.x86_64-linux.run {
        src = ./.;

        hooks = {
          nixfmt-rfc-style.enable = true;
          deadnix.enable = true;
          statix.enable = true;
        };
      };

      devShells.x86_64-linux.default = mkCrossShell {
        nativeBuildInputs = [
          xwin
          pkgs.just
          pkgs.curl
          pkgs.jq
        ];
        inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
      };
    };
}
